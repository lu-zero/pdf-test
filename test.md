# Test

| a   | b   | c    |
| --- | --- | ---- |
| 10  | 100 | 1000 |
| 10  | 100 | 1000 |

### Explanation

This paragraph is showing **rich formatting** and _improved text_ rendering.

Here is a code block with `syntax highlighting`.

```c
void main() {
    int a;
}
```

## Add a paragraph

This is some text

```mermaid
graph LR;
    A[Fork the Repo] --> B[Create ];
    A-->C;
    B-->D;
    C-->D;
```

## Add another paragraph

```mermaid
graph TD;
    A[Fork the Repo] --> B[Create ];
    A-->C;
    B-->D;
    C-->D;
```

And another graph.
